#include "Graphics/Camera.hpp"
#include "Graphics/Renderer.hpp"
#include "Graphics/Shader.hpp"

int main() {
  // meshToFile(createMesh(100), createIndex(100));

  ge::Window window("test", 1980, 1080);
  window.init();
  ge::Clock clock;
  ge::Shader shader("Common/vertex.glsl", "Common/fragment.glsl");
  ge::Renderer renderer(shader.shaderProgram);
  ge::Camera camera(shader.shaderProgram);
  ge::Physics *phy = new ge::Physics;

  std::vector<ge::Entity *> entities;

  entities.emplace_back(
      new ge::Entity(new Mesh("Obj/test.dae"), glm::vec3(0.0, 0.0, 0.0)));
  // entities.back()->textureID = renderer.genTexture("Obj/test2.jpg");
  entities.emplace_back(
      new ge::Entity(new Mesh("Obj/wooden.obj"), glm::vec3(30.0, 0.0, 30.0)));
  entities.back()->textureID = renderer.genTexture("Obj/Wood_Tower_Col.jpg");
  entities.emplace_back(
      new ge::Entity(new Mesh("Obj/mesh.obj"), glm::vec3(0.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(
      new ge::Entity(new Mesh("Obj/mesh.obj"), glm::vec3(200.0f, 0.0f, 0.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(new ge::Entity(new Mesh("Obj/mesh.obj"),
				       glm::vec3(200.0f, 0.0f, 200.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(
      new ge::Entity(new Mesh("Obj/mesh.obj"), glm::vec3(0.0f, 0.0f, 200.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(
      new ge::Entity(new Mesh("Obj/mesh.obj"), glm::vec3(400.0f, 0.0f, 0.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(new ge::Entity(new Mesh("Obj/mesh.obj"),
				       glm::vec3(400.0f, 0.0f, 200.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(new ge::Entity(new Mesh("Obj/mesh.obj"),
				       glm::vec3(400.0f, 0.0f, 400.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(new ge::Entity(new Mesh("Obj/mesh.obj"),
				       glm::vec3(200.0f, 0.0f, 400.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");
  entities.emplace_back(
      new ge::Entity(new Mesh("Obj/mesh.obj"), glm::vec3(0.0f, 0.0f, 400.0f)));
  entities.back()->textureID = renderer.genTexture("Obj/grass.jpg");

  camera.m_ent = entities.front();
  entities.front()->m_phy = phy;

  while (!window.shouldClose()) {
    renderer.clear();
    shader.use();

    for (auto &ent : entities)
      renderer.render(ent);

    phy->update(clock.delta * 1000.0f);
    entities.front()->update(glm::vec3((float)phy->trans.getOrigin().getX(),
				       (float)phy->trans.getOrigin().getY(),
				       (float)phy->trans.getOrigin().getZ()),
			     glm::quat(phy->quat.getW(), phy->quat.getX(),
				       phy->quat.getY(), phy->quat.getZ()));

    camera.handleInput(window, clock.delta);
    clock.update();
    window.update();
  }

  for (auto &ent : entities) {
    if (ent->textureID != 0)
      glDeleteTextures(1, &ent->textureID);
    delete ent;
  }

  delete phy;
  window.terminate();
}
