#include "Renderer.hpp"
#define STB_IMAGE_IMPLEMENTATION
#include "../Common/stb_image.h"

namespace ge {
Renderer::Renderer(int shaderProgram) {
  glGenVertexArrays(1, &VAO);
  glGenBuffers(1, &VBO);
  glGenBuffers(1, &EBO);
  modelID = glGetUniformLocation(shaderProgram, "model");
  textID = glGetUniformLocation(shaderProgram, "textured");
}

void Renderer::render(Entity *entity) {
  glUniformMatrix4fv(modelID, 1, GL_FALSE, &entity->m_model[0][0]);

  if (entity->textureID != 0) {
    glBindTexture(GL_TEXTURE_2D, entity->textureID);
    glUniform1i(textID, 1);
  } else
    glUniform1i(textID, 0);

  glBindVertexArray(VAO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER,
	       sizeof(float) * entity->m_mesh->o_vertices.size(),
	       &entity->m_mesh->o_vertices.front(), GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
	       sizeof(unsigned int) * entity->m_mesh->o_indices.size(),
	       &entity->m_mesh->o_indices.front(), GL_STATIC_DRAW);

  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)0);

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
			(void *)(3 * sizeof(float)));

  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float),
			(void *)(6 * sizeof(float)));

  glDrawElements(GL_TRIANGLES,
		 sizeof(unsigned int) * entity->m_mesh->o_indices.size(),
		 GL_UNSIGNED_INT, 0);
}

void Renderer::clear() {
  glClearColor(35 / 255.0f, 120 / 255.0f, 120 / 255.0f, 1);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

unsigned int Renderer::genTexture(const char *fileName) {
  stbi_set_flip_vertically_on_load(true);
  int width = 0, height = 0, channels = 0;
  unsigned int textureID;
  unsigned char *data = stbi_load(fileName, &width, &height, &channels, 0);
  if (!data)
    return 0;
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		  GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
	       GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  stbi_image_free(data);
  return textureID;
}

Renderer::~Renderer() {
  glDeleteBuffers(1, &VBO);
  glDeleteBuffers(1, &EBO);
  glDeleteVertexArrays(1, &VAO);
}
} // namespace ge
