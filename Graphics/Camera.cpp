#include "Camera.hpp"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace ge {
Camera::Camera(int shaderProgram) {
  position = glm::vec3(-10, 5, -10);
  direction = glm::vec3(1.0f);
  worldUp = glm::vec3(0.0f, 1.0f, 0.0f);
  worldX = glm::vec3(1.0f, 0.0f, 0.0f);
  speed = 25.0f;
  mouseSpeed = 0.0005f;
  matrixID = glGetUniformLocation(shaderProgram, "VP");
}

void Camera::handleInput(Window &window, float delta) {

  glfwGetCursorPos(window.window, &xpos, &ypos);
  glfwSetCursorPos(window.window, window.m_width / 2, window.m_height / 2);

  deltaX += mouseSpeed * float(window.m_width / 2 - xpos);
  deltaY += mouseSpeed * float(window.m_height / 2 - ypos);

  if (thirdPerson) {
    view = glm::translate(glm::mat4(1.0f), -position);
    rotation = glm::angleAxis(deltaX, worldUp);
    view = glm::toMat4(glm::inverse(rotation)) * view;
    rotation = glm::angleAxis(deltaY, worldX);
    view = glm::toMat4(glm::inverse(rotation)) * view;

    direction = -glm::vec3(glm::inverse(view)[2]);
    right = glm::vec3(glm::inverse(view)[0]);

    if (glfwGetKey(window.window, GLFW_KEY_W) == GLFW_PRESS) {
      position += direction * speed * delta;
    }
    if (glfwGetKey(window.window, GLFW_KEY_S) == GLFW_PRESS) {
      position -= direction * speed * delta;
    }
    if (glfwGetKey(window.window, GLFW_KEY_D) == GLFW_PRESS) {
      position += right * speed * delta;
    }
    if (glfwGetKey(window.window, GLFW_KEY_A) == GLFW_PRESS) {
      position -= right * speed * delta;
    }
    if (glfwGetKey(window.window, GLFW_KEY_R) == GLFW_PRESS) {
      thirdPerson = !thirdPerson;
    }
  } else {
    view = glm::translate(glm::mat4(1.0f), -glm::vec3(0., 10., 20.));
    // view = glm::translate(glm::mat4(1.0f), -glm::vec3(0.,0.,0.));

    rotation = glm::angleAxis(deltaX, worldUp);
    m_ent->m_model *= glm::toMat4(rotation);
    // view = glm::toMat4(glm::inverse(rotation)) * view;
    rotation = glm::angleAxis(deltaY, worldX);
    m_ent->m_model *= glm::toMat4(rotation);
    // view = glm::toMat4(glm::inverse(rotation)) * view;
    view *= glm::inverse(m_ent->m_model);

    // direction = -glm::vec3(glm::inverse(view)[2]);
    direction = -glm::vec3(m_ent->m_model[2]);
    right = glm::vec3(m_ent->m_model[0]);

    if (glfwGetKey(window.window, GLFW_KEY_W) == GLFW_PRESS) {
      m_ent->m_phy->cube->activate();
      m_ent->m_phy->cube->setDamping(0, 0);
      m_ent->m_phy->cube->applyCentralForce(
	  btVector3(direction[0], direction[1], direction[2]) * 100.0f);
    }
    if (glfwGetKey(window.window, GLFW_KEY_W) == GLFW_RELEASE) {
      m_ent->m_phy->cube->clearForces();
      m_ent->m_phy->cube->setDamping(0.9, 0.9);
    }
    if (glfwGetKey(window.window, GLFW_KEY_S) == GLFW_PRESS) {
      m_ent->m_phy->cube->activate();
      m_ent->m_phy->cube->setDamping(0, 0);
      m_ent->m_phy->cube->applyCentralForce(
	  -btVector3(direction[0], direction[1], direction[2]) * 100.0f);
    }
    if (glfwGetKey(window.window, GLFW_KEY_D) == GLFW_PRESS) {
      m_ent->m_phy->cube->activate();
      m_ent->m_phy->cube->setDamping(0, 0);
      m_ent->m_phy->cube->applyCentralForce(
	  btVector3(right[0], right[1], right[2]) * 100.0f);
    }
    if (glfwGetKey(window.window, GLFW_KEY_A) == GLFW_PRESS) {
      m_ent->m_phy->cube->activate();
      m_ent->m_phy->cube->setDamping(0, 0);
      m_ent->m_phy->cube->applyCentralForce(
	  -btVector3(right[0], right[1], right[2]) * 100.0f);
    }
    if (glfwGetKey(window.window, GLFW_KEY_SPACE) == GLFW_PRESS) {
      m_ent->m_phy->cube->activate();
      m_ent->m_phy->cube->setDamping(0, 0);
      m_ent->m_phy->cube->applyCentralForce(
	  btVector3(worldUp[0], worldUp[1], worldUp[2]) * 100.0f);
    }
    if (glfwGetKey(window.window, GLFW_KEY_R)) {
      thirdPerson = !thirdPerson;
    }
  }

  projection = glm::perspective(glm::radians(45.0f),
				(float)window.m_width / (float)window.m_height,
                                0.1f, 1000.0f);

  vp = projection * view;
  glUniformMatrix4fv(matrixID, 1, GL_FALSE, &vp[0][0]);
}

Camera::~Camera() {}
} // namespace ge
