#pragma once
#include <string>
#include <fstream>

namespace ge
{
	class Shader
	{
		public:
			Shader(const char* srcvertexShader, const char* srcfragShader);
			~Shader();
			void use();

			int vertexShader, fragShader, geomShader;
			int shaderProgram;
	};
}
