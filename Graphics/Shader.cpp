#include "Shader.hpp"

#include <GL/glew.h>

namespace ge
{
	Shader::Shader(const char* srcvertexShader, const char* srcfragShader)
	{
		std::ifstream vs(srcvertexShader);
		std::ifstream fs(srcfragShader);

		std::string vsl;
		std::string line = "";
		while(getline(vs, line))
			vsl.append(line + '\n');
			
		std::string fsl;
		line = "";
		while(getline(fs, line))
			fsl.append(line + '\n');

		const char* vsdata = vsl.c_str();
		const char* fsdata = fsl.c_str();

		int success;
		char log[512];
		
		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vertexShader, 1, &vsdata, NULL);
		glCompileShader(vertexShader);
		glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
		if(!success)
		{
			puts("vertex shader error");
			glGetShaderInfoLog(vertexShader, 512, NULL, log);
			puts(log);
		}

		fragShader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fragShader, 1, &fsdata, NULL);
		glCompileShader(fragShader);
		glGetShaderiv(fragShader, GL_COMPILE_STATUS, &success);
		if(!success)
		{
			puts("fragment shader error");
			glGetShaderInfoLog(fragShader, 512, NULL, log);
			puts(log);
		}

		shaderProgram = glCreateProgram();
		glAttachShader(shaderProgram, vertexShader);
		glAttachShader(shaderProgram, fragShader);
		glLinkProgram(shaderProgram);
		glDeleteShader(vertexShader);
		glDeleteShader(fragShader);
		vs.close();
		fs.close();
	}
	void Shader::use()
	{
		glUseProgram(shaderProgram);
	}

	Shader::~Shader()
	{
		glDeleteProgram(shaderProgram);
	}
}

