#pragma once 

#include "../Window/Window.hpp"
#include "../Entities/Entity.hpp"

namespace ge
{
	class Camera
	{
		public:
			Camera(int shaderProgram);
			~Camera();
			void handleInput(Window& window, float delta);
			int matrixID;
			double xpos, ypos;
			float deltaX, deltaY, speed, mouseSpeed;
			bool thirdPerson = true;
			Entity* m_ent;
			glm::quat rotation; 
			glm::mat4 projection, view, vp;
			glm::vec3 worldUp, worldX;
			glm::vec3 position, direction, right;
	};
}
