#pragma once

#include "../Entities/Entity.hpp"
#include <GL/glew.h>

#include <vector>

namespace ge {
class Renderer {
public:
  Renderer(int shaderProgram);
  ~Renderer();
  void render(Entity *entity);
  void clear();
  unsigned int genTexture(const char *fileName);

  unsigned int VBO, VAO, EBO;
  int modelID, textID;
};
} // namespace ge
