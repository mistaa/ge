#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

namespace ge {
class Window {
public:
  Window(const char *title, const int width, const int height);
  ~Window();
  void init();
  void update();
  void terminate();
  bool inline shouldClose() { return glfwWindowShouldClose(window); }
  static int m_width, m_height;
  GLFWwindow *window = nullptr;

private:
  const char *m_title;
};

class Clock {
public:
  Clock();
  ~Clock();
  float previousTime;
  float delta = 0;
  float timeElapsed = 0;
  int frames = 0;
  inline void update() {
    double currentTime = glfwGetTime();
    delta = float(currentTime - previousTime);
    frames++;
    timeElapsed += delta;
    if (timeElapsed >= 1.0f) {
      std::cout << frames << "\n";
      std::cout << delta * 1000.0f << "\n";
      frames = 0;
      timeElapsed = 0;
    }
    previousTime = currentTime;
  }
};
} // namespace ge
