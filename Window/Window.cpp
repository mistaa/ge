#include "Window.hpp"
#include <GLFW/glfw3.h>

int ge::Window::m_width, ge::Window::m_height;

void key_callb(GLFWwindow *window, int key, int scancode, int action,
               int mods) {
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  glViewport(0, 0, ge::Window::m_width, ge::Window::m_height);
}

namespace ge {
Window::Window(const char *title, const int width, const int height) {
  m_width = width;
  m_height = height;
  m_title = title;
}
void Window::init() {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_SAMPLES, 4);
  window = glfwCreateWindow(m_width, m_height, m_title, glfwGetPrimaryMonitor(), NULL);
  glfwMakeContextCurrent(window);
  glfwSwapInterval(1);
  glfwSetKeyCallback(window, key_callb);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glewInit();
  glEnable(GL_MULTISAMPLE);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glEnable(GL_CULL_FACE);
  glEnable(GL_FRAMEBUFFER_SRGB);
}
void Window::update() {
  glfwSwapBuffers(window);
  glfwPollEvents();
}
void Window::terminate() {
  glfwDestroyWindow(window);
  glfwTerminate();
}

Window::~Window() {}

Clock::Clock() { previousTime = glfwGetTime(); }

Clock::~Clock() {}
} // namespace ge
