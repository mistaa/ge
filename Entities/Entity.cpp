#include "Entity.hpp"

namespace ge {
Entity::Entity(Mesh *mesh, glm::vec3 position) {
  m_mesh = mesh;
  m_position = position;
  m_model = glm::translate(glm::mat4(1.0f), m_position);
  textureID = 0;
}
void Entity::update(glm::vec3 newPos, glm::quat quaternion) {
  glm::mat4 newRotation = glm::toMat4(glm::normalize(quaternion));
  m_model = glm::translate(glm::mat4(1.0f), newPos);
  m_model *= newRotation;
  m_position = newPos;
}
Entity::~Entity() { delete m_mesh; }
} // namespace ge
