#include "Physics.hpp"

namespace ge {

Physics::Physics() {
  collisionConfiguration = new btDefaultCollisionConfiguration();
  dispatcher = new btCollisionDispatcher(collisionConfiguration);
  overlappingPairCache = new btDbvtBroadphase();
  solver = new btSequentialImpulseConstraintSolver;
  dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache,
                                              solver, collisionConfiguration);

  dynamicsWorld->setGravity(btVector3(0, -10, 0));

  btAlignedObjectArray<btCollisionShape *> collisionShapes;

  groundShape =
      new btBoxShape(btVector3(btScalar(300), btScalar(20), btScalar(300)));

  collisionShapes.push_back(groundShape);

  btTransform groundTransform;
  groundTransform.setIdentity();
  groundTransform.setOrigin(btVector3(300, -20, 300));

  btScalar mass(0.);

  bool isDynamic = (mass != 0.f);

  btVector3 localInertia(0, 0, 0);
  if (isDynamic)
    groundShape->calculateLocalInertia(mass, localInertia);

  myMotionState = new btDefaultMotionState(groundTransform);
  btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState,
                                                  groundShape, localInertia);
  body = new btRigidBody(rbInfo);

  dynamicsWorld->addRigidBody(body);

  colShape = new btBoxShape(btVector3(5, 1, 3));

  collisionShapes.push_back(colShape);

  btTransform startTransform;
  startTransform.setIdentity();
  mass = 2.f;

  isDynamic = (mass != 0.f);

  localInertia = {0, 0, 0};
  if (isDynamic)
    colShape->calculateLocalInertia(mass, localInertia);

  startTransform.setOrigin(btVector3(200, 5, 200));

  myMotionState = new btDefaultMotionState(startTransform);
  rbInfo = {mass, myMotionState, colShape, localInertia};
  cube = new btRigidBody(rbInfo);

  dynamicsWorld->addRigidBody(cube);
}

void Physics::update(float delta) {
  dynamicsWorld->stepSimulation(1.f / 60.f);
  btCollisionObject *obj = dynamicsWorld->getCollisionObjectArray()[1];
  btRigidBody *body = btRigidBody::upcast(obj);
  body->getMotionState()->getWorldTransform(trans);
  quat = trans.getRotation();
}

Physics::~Physics() {
  delete body;
  delete cube;
  delete colShape;
  delete groundShape;
  delete dynamicsWorld;
  delete solver;
  delete overlappingPairCache;
  delete dispatcher;
  delete collisionConfiguration;
}
} // namespace ge
