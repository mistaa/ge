#pragma once

#include "Physics.hpp"
#include "MeshGenerator.hpp"
#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

namespace ge
{
	class Entity
	{
		public:
			Entity(Mesh* mesh, glm::vec3 position);
			~Entity();
			void update(glm::vec3 newPos, glm::quat quaternion);
			Mesh* m_mesh;
			Physics* m_phy;
			glm::vec3 m_position;
			glm::mat4 m_model;
			unsigned int textureID; 
	};
}
