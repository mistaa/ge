#pragma once

#include "btBulletDynamicsCommon.h"

namespace ge
{
	class Physics
	{
		public:
			Physics();
			~Physics();
			btTransform trans;
			btQuaternion quat;
			btRigidBody* cube;
			btRigidBody* body; 
			btDiscreteDynamicsWorld* dynamicsWorld;
			btDefaultCollisionConfiguration* collisionConfiguration;
			btCollisionDispatcher* dispatcher;
			btBroadphaseInterface* overlappingPairCache;
			btSequentialImpulseConstraintSolver* solver;
			btDefaultMotionState* myMotionState;
			btCollisionShape* groundShape;
			btCollisionShape* colShape;
			void update(float delta);
	};
}
