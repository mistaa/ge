#pragma once

#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <fstream>
#include <ctime>
#include <cstdio>

class Mesh
{
	public:
		Mesh(const char* fileName);
		~Mesh();
		std::vector<float> o_vertices;
		std::vector<float> normals;
		std::vector<unsigned int> o_indices;
		void processNode(const aiScene* scene, const aiNode* node);
};

std::vector<float> createMesh(int dim);
std::vector<unsigned int> createIndex(int dim);
void meshToFile(std::vector<float> res, std::vector<unsigned int> index);
