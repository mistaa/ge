#include "MeshGenerator.hpp"

std::vector<float> createMesh(int dim)
{
	std::vector<float> res;
	srand(time(0));
	float x = 0;
	float z = 0;
	for(int i = 0; i < dim+1; i++, z+=2.0f)
	{
		if(x > dim * 2)
			x = 0.0f;
		for(int j = 0; j < dim+1; j++, x+=2.0f)
		{
			float y = rand() % 2 / 2.5f;
			res.push_back(x);
			res.push_back(y);
			res.push_back(z);
		}
	}
	return res;
}

std::vector<unsigned int> createIndex(int dim)
{
	std::vector<unsigned int> index;

	for(auto o = 0, j = 0; j < dim; j++, o+=dim+1)
	{
		for(auto i = 0; i < dim; i++)
		{
			index.push_back(i+o);
			index.push_back(i+dim+1+o);
			index.push_back(i+1+o);

			index.push_back(i+1+o);
			index.push_back(i+dim+1+o);
			index.push_back(i+dim+2+o);
		}
	}
	return index;
}

void meshToFile(std::vector<float> res, std::vector<unsigned int> index)
{
	int s = 1;
	std::ofstream mesh("Obj/mesh.obj");
	for(std::size_t i = 0; i < res.size(); i+=3)
	{
		mesh << "v " << res[i] << " " << res[i+1] << " " << res[i+2] << "\n";
	}
	for(std::size_t i = 0; i < index.size(); i+=3, s++)
	{
		mesh << "s " << s << "\n";
		mesh << "f " << index[i] + 1 << "// " << index[i+1] + 1 << "// " << index[i+2] + 1 << "//\n";
	}
	mesh.close();
}

Mesh::Mesh(const char* fileName)
{
	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(fileName, aiProcess_Triangulate 
			| aiProcess_GenSmoothNormals | aiProcess_FixInfacingNormals 
			| aiProcess_FindInvalidData | aiProcess_ImproveCacheLocality 
			| aiProcess_OptimizeMeshes); 
	if(!scene)
	{
		puts(importer.GetErrorString());
		return;
	}
	processNode(scene, scene->mRootNode);
}
void Mesh::processNode(const aiScene* scene, const aiNode* node)
{
	int turn = 1;
	for(unsigned int i = 0; i < node->mNumMeshes; i++)
	{
		aiMesh* mesh = scene->mMeshes[i];
		for(unsigned int j = 0; j < mesh->mNumVertices; j++)
		{
			o_vertices.push_back(mesh->mVertices[j].x);
			o_vertices.push_back(mesh->mVertices[j].y);
			o_vertices.push_back(mesh->mVertices[j].z);
			if(mesh->HasNormals())
			{
				o_vertices.push_back(mesh->mNormals[j].x);
				o_vertices.push_back(mesh->mNormals[j].y);
				o_vertices.push_back(mesh->mNormals[j].z);
			}
			if(mesh->HasTextureCoords(0))
			{
				o_vertices.push_back(mesh->mTextureCoords[0][j].x);
				o_vertices.push_back(mesh->mTextureCoords[0][j].y);
			} 
			else switch(turn)
			{
				case 1:
					o_vertices.push_back(0.0f);
					o_vertices.push_back(0.0f);
					turn++; break;
				case 2:
					o_vertices.push_back(1.0f);
					o_vertices.push_back(0.0f);
					turn++; break;
				case 3:
					o_vertices.push_back(0.0f);
					o_vertices.push_back(1.0f);
					turn++; break;
				case 4:
					o_vertices.push_back(0.0f);
					o_vertices.push_back(1.0f);
					turn++; break;
				case 5:
					o_vertices.push_back(1.0f);
					o_vertices.push_back(0.0f);
					turn++; break;
				case 6:
					o_vertices.push_back(1.0f);
					o_vertices.push_back(1.0f);
					turn = 1; break;
			}
		}
		if(mesh->HasFaces())
		{
			for(unsigned int x = 0; x < mesh->mNumFaces; x++)
			{
				const aiFace& face = mesh->mFaces[x];
				o_indices.push_back(face.mIndices[0]);
				o_indices.push_back(face.mIndices[1]);
				o_indices.push_back(face.mIndices[2]);
			}
		}
	}
	for(unsigned int i = 0; i < node->mNumChildren; i++)
		processNode(scene, node->mChildren[i]);
}

Mesh::~Mesh()
{ }

