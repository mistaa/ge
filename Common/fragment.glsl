#version 330 core
in vec3 normals;
in vec3 fragPos;
in vec2 texPos;

uniform int textured;
uniform sampler2D tex;

out vec4 FragColor;

void main()
{
	vec3 lightPos = vec3(300.0, 300.0, 300.0);
	vec3 lightColor = vec3(100.0, 100.0, 100.0);
	vec3 ambient = 0.5 * lightColor;
	vec3 objColor = vec3(1.0, 1.0, 1.0);

	vec3 norm = normalize(normals);
	vec3 lightDir = normalize(lightPos - fragPos);

	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * lightColor;

	float distance = length(lightPos - fragPos);
	float attenuation = 1.0 / (1.0 + 0.1 * distance + 0.01 * (distance * distance));
	vec3 result = (ambient + diffuse) * attenuation;
	if(textured == 1)
	{
		FragColor = texture(tex, texPos) * vec4(result, 1.0);
	}
	else
	{
		FragColor = vec4(result * objColor, 1.0);
	}
};
