#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormals;
layout (location = 2) in vec2 aTex;

uniform mat4 VP;
uniform mat4 model;

out vec3 normals;
out vec3 fragPos;
out vec2 texPos;

void main()
{
	fragPos = vec3(model * vec4(aPos, 1.0));
	gl_Position = VP * model * vec4(aPos, 1.0f);
	normals = mat3(transpose(inverse(model))) * aNormals;
	texPos = aTex;
};
